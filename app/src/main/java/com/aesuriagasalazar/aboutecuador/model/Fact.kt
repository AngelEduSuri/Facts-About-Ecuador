package com.aesuriagasalazar.aboutecuador.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

/**
 * Data class representing a fact
 *
 * @property title Title of the fact.
 * @property description Description of the fact.
 * @property image Image of the fact in drawable format.
 */
data class Fact(
    @StringRes val title: Int,
    @StringRes val description: Int,
    @DrawableRes val image: Int,
)